const result = [
  { text: 'Lavar o carro', id: new Date().getTime() },
  { text: 'Dar vacina na Alice', id: new Date().getTime() }
]

const listNotes = () => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(result)
    }, 2000)
  })
}

const searchNotes = text => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      if (text === '') {
        return resolve(result)
      }
      result.map(note => {
        if (note.text.includes(text)) {
          return resolve([note])
        }
      })
      return resolve([
        {
          text: 'Nenhuma Nota Encontrada !',
          id: new Date().getTime()
        }
      ])
    }, 2000)
  })
}

const saveNotes = text => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      result.unshift({
        text: text,
        id: new Date().getTime()
      })
      resolve()
    }, 2000)
  })
}

const delNotes = text => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      result.pop({ text })
      resolve()
    }, 2000)
  })
}

module.exports = {
  listNotes,
  saveNotes,
  delNotes,
  searchNotes
}
