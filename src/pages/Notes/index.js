import React, { Component } from 'react'
import Header from '../../components/Header'

import {
  listNotes,
  saveNotes,
  delNotes,
  searchNotes
} from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  state = {
    notes: [],
    loading: true,
    loadingSave: false,
    loadingDel: false,
    message: 'Carregando anotações...',
    text: ''
  }

  async componentDidMount () {
    this.setState({
      notes: await listNotes(),
      loading: false
    })
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = event => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async note => {
    if (note.charCode === 13) {
      await saveNotes(this.state.text)
      this.setState({
        notes: [],
        loadingSave: true,
        message: ''
      })
      this.setState({
        notes: [].concat(...(await listNotes())), // ,
        loadingSave: false,
        new: false,
        text: ''
      })
    }
    await saveNotes(this.state.text)
    this.setState({
      notes: [],
      loadingSave: true,
      message: ''
    })
    this.setState({
      notes: [].concat(...(await listNotes())), // ,
      loadingSave: false,
      new: false,
      text: ''
    })
  }

  delNote = async note => {
    this.setState({
      notes: [],
      message: 'Excluindo Nota...'
    })

    await delNotes(note)
    this.setState({
      notes: await listNotes(),
      loadingDel: false,
      new: false,
      text: ''
    })
  }

  searchNote = async note => {
    if (note.charCode === 13) {
      this.setState({
        notes: [],
        message: 'Buscando...'
      })
      let test = await searchNotes(note.target.value)
      this.setState({
        notes: [].concat(...test),
        new: false,
        text: ''
      })
    }
  }

  render () {
    return (
      <div>
        <Header />
        <div className='content'>
          <div className='filters'>
            <div className='left'>
              <input
                type='search'
                placeholder='Procurar...'
                onKeyPress={this.searchNote}
              />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            {this.state.loading && (
              <div className='item item-empty'>{this.state.message}</div>
            )}
            {!this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>{this.state.message}</div>
            )}
            {this.state.new && (
              <div className='item'>
                <input
                  onChange={this.editText}
                  className='left'
                  type='text'
                  placeholder='Digite sua anotação'
                />
                <div className='right'>
                  {!this.state.loadingSave && (
                    <button onClick={this.saveNote} onKeyPress={this.saveNote}>
                      Salvar
                    </button>
                  )}
                  {this.state.loadingSave && <span>Salvando...</span>}
                </div>
              </div>
            )}
            {this.state.notes.map((note, i) => (
              <div className='item' key={i}>
                <input
                  onChange={this.editText}
                  className='left'
                  type='text'
                  defaultValue={note.text}
                  placeholder='Digite sua anotação'
                />
                <div className='right'>
                  {!note.id && <button onClick={this.saveNote}>Salvar</button>}
                  {!this.state.loadingDel && note.id && (
                    <button onClick={this.delNote}>Excluir</button>
                  )}
                  {note.id && <button onClick={this.editNote}>Editar</button>}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
